import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JTextField;

import Helpers.Funcionalidades;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Calculator {

	private JFrame frame;
	private JTextField txtNumeroIngresa;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Calculator window = new Calculator();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Calculator() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 490, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblDesafioNumero = new JLabel("0");
		lblDesafioNumero.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblDesafioNumero.setBounds(245, 11, 115, 34);
		frame.getContentPane().add(lblDesafioNumero);

		txtNumeroIngresa = new JTextField();
		txtNumeroIngresa.setEditable(false);
		txtNumeroIngresa.setBounds(10, 11, 210, 34);
		frame.getContentPane().add(txtNumeroIngresa);
		txtNumeroIngresa.setColumns(10);

		JButton btnFuncion = new JButton("Iniciar");
		btnFuncion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (btnFuncion.getText().equals("Iniciar")) {
					// Obtengo un numero random desde el helper
					lblDesafioNumero.setText(String.valueOf(Funcionalidades.NumeroRandom()));
					btnFuncion.setText("Finalizar");
				} else {
					// Regreso al punto de partida ya que el usuario dio finalizar
					if (Funcionalidades.CheckDivCero(txtNumeroIngresa.getText())) {
						Integer resultado = Funcionalidades.ResolveMath(txtNumeroIngresa.getText());
						if(Funcionalidades.CheckResultado(resultado,lblDesafioNumero.getText()))
						{
							JOptionPane.showMessageDialog(null, "Has acertado en el resultado!!!!");
						}
						else
						{
							JOptionPane.showMessageDialog(null, "No has acertado en el resultado!!!!");
						}
						
						lblDesafioNumero.setText("0");
						txtNumeroIngresa.setText("");
						btnFuncion.setText("Iniciar");
					}
					else
					{
						JOptionPane.showMessageDialog(null, "No es posible dividir por 0 (cero)!!!!");
						lblDesafioNumero.setText("0");
						txtNumeroIngresa.setText("");
						btnFuncion.setText("Iniciar");
					}
				}

			}
		});

		JButton btnUno = new JButton("1");
		btnUno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Funcionalidades.CheckAppState(btnFuncion.getText()))
					txtNumeroIngresa.setText(txtNumeroIngresa.getText() + String.valueOf("1"));
			}
		});
		btnUno.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnUno.setBounds(10, 199, 62, 52);
		frame.getContentPane().add(btnUno);

		JButton btnDos = new JButton("2");
		btnDos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Funcionalidades.CheckAppState(btnFuncion.getText()))
					txtNumeroIngresa.setText(txtNumeroIngresa.getText() + String.valueOf("2"));
			}
		});
		btnDos.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnDos.setBounds(82, 199, 62, 52);
		frame.getContentPane().add(btnDos);

		JButton btnTres = new JButton("3");
		btnTres.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Funcionalidades.CheckAppState(btnFuncion.getText()))
					txtNumeroIngresa.setText(txtNumeroIngresa.getText() + String.valueOf("3"));
			}
		});
		btnTres.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnTres.setBounds(158, 199, 62, 52);
		frame.getContentPane().add(btnTres);

		JButton btnCuatro = new JButton("4");
		btnCuatro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Funcionalidades.CheckAppState(btnFuncion.getText()))
					txtNumeroIngresa.setText(txtNumeroIngresa.getText() + String.valueOf("4"));
			}
		});
		btnCuatro.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnCuatro.setBounds(10, 136, 62, 52);
		frame.getContentPane().add(btnCuatro);

		JButton btnCinco = new JButton("5");
		btnCinco.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Funcionalidades.CheckAppState(btnFuncion.getText()))
					txtNumeroIngresa.setText(txtNumeroIngresa.getText() + String.valueOf("5"));
			}
		});
		btnCinco.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnCinco.setBounds(82, 136, 62, 52);
		frame.getContentPane().add(btnCinco);

		JButton btnSeis = new JButton("6");
		btnSeis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Funcionalidades.CheckAppState(btnFuncion.getText()))
					txtNumeroIngresa.setText(txtNumeroIngresa.getText() + String.valueOf("6"));
			}
		});
		btnSeis.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnSeis.setBounds(158, 136, 62, 52);
		frame.getContentPane().add(btnSeis);

		JButton btnSiete = new JButton("7");
		btnSiete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Funcionalidades.CheckAppState(btnFuncion.getText()))
					txtNumeroIngresa.setText(txtNumeroIngresa.getText() + String.valueOf("7"));
			}
		});
		btnSiete.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnSiete.setBounds(10, 73, 62, 52);
		frame.getContentPane().add(btnSiete);

		JButton btnOcho = new JButton("8");
		btnOcho.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Funcionalidades.CheckAppState(btnFuncion.getText()))
					txtNumeroIngresa.setText(txtNumeroIngresa.getText() + String.valueOf("8"));
			}
		});
		btnOcho.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnOcho.setBounds(82, 73, 62, 52);
		frame.getContentPane().add(btnOcho);

		JButton btnNueve = new JButton("9");
		btnNueve.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Funcionalidades.CheckAppState(btnFuncion.getText()))
					txtNumeroIngresa.setText(txtNumeroIngresa.getText() + String.valueOf("9"));
			}
		});
		btnNueve.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnNueve.setBounds(158, 73, 62, 52);
		frame.getContentPane().add(btnNueve);

		JButton btnMas = new JButton("+");
		btnMas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Funcionalidades.CheckAppState(btnFuncion.getText())) {
					if (Funcionalidades.CheckOperador(txtNumeroIngresa.getText()))
						txtNumeroIngresa.setText(txtNumeroIngresa.getText() + String.valueOf("+"));
				}
			}
		});
		btnMas.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnMas.setBounds(330, 73, 62, 52);
		frame.getContentPane().add(btnMas);

		JButton btnMenos = new JButton("-");
		btnMenos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Funcionalidades.CheckAppState(btnFuncion.getText())) {
					if (Funcionalidades.CheckOperador(txtNumeroIngresa.getText()))
						txtNumeroIngresa.setText(txtNumeroIngresa.getText() + String.valueOf("-"));
				}
			}
		});
		btnMenos.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnMenos.setBounds(402, 73, 62, 52);
		frame.getContentPane().add(btnMenos);

		JButton btnPor = new JButton("*");
		btnPor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Funcionalidades.CheckAppState(btnFuncion.getText())) {
					if (Funcionalidades.CheckOperador(txtNumeroIngresa.getText()))
						txtNumeroIngresa.setText(txtNumeroIngresa.getText() + String.valueOf("*"));
				}
			}
		});
		btnPor.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnPor.setBounds(330, 136, 62, 52);
		frame.getContentPane().add(btnPor);

		JButton btnDiv = new JButton("/");
		btnDiv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Funcionalidades.CheckAppState(btnFuncion.getText())) {
					if (Funcionalidades.CheckOperador(txtNumeroIngresa.getText()))
						txtNumeroIngresa.setText(txtNumeroIngresa.getText() + String.valueOf("/"));
				}
			}
		});
		btnDiv.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnDiv.setBounds(402, 136, 62, 52);
		frame.getContentPane().add(btnDiv);

		btnFuncion.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnFuncion.setBounds(330, 10, 134, 52);
		frame.getContentPane().add(btnFuncion);

		JButton btnClear = new JButton("Borrar");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNumeroIngresa.setText("");
			}
		});
		btnClear.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnClear.setBounds(330, 199, 134, 52);
		frame.getContentPane().add(btnClear);
		
		JButton btnCero = new JButton("0");
		btnCero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Funcionalidades.CheckAppState(btnFuncion.getText()))
					txtNumeroIngresa.setText(txtNumeroIngresa.getText() + String.valueOf("0"));
			}
		});
		btnCero.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnCero.setBounds(230, 199, 62, 52);
		frame.getContentPane().add(btnCero);
	}
}
