package Helpers;

public class Calculador {

	public Calculador() {}
	
	double Sumar(double a, double b) {
		double respuesta = a + b;
		return respuesta;
	}

	double Restar(double a, double b) {
		double respuesta = a - b;
		return respuesta;
	}

	double Multiplicar(double a, double b) {
		double respuesta = a * b;
		return respuesta;
	}

	double Dividir(double a, double b) {
		double respuesta = a / b;
		return respuesta;
	}
}