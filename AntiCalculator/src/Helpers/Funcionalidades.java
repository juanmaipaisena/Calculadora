package Helpers;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

public class Funcionalidades {
	public static int NumeroRandom() {
		Random random = new Random();

		return random.nextInt(50000) + 1;
	}

	public static boolean CheckOperador(String cadena) {
		// veo que no sea el primer caracter ingresado
		if (cadena.length() == 0)
			return false;

		// obtengo el ultimo caracter ingresado
		String lastChar = cadena.substring(cadena.length() - 1);

		// evaluo que no ingrese una cadena de operadores
		if (lastChar.equals("+") || lastChar.equals("-") || lastChar.equals("*") || lastChar.equals("/"))
			return false;
		else
			return true;
	}

	public static boolean CheckAppState(String estado) {
		// si no inicio la aplicacion, osea que su texto es iniciar, no dejo que ingrese
		// ningun numero u operador
		if (estado.equals("Iniciar"))
			return false;
		else
			return true;
	}

	private static ArrayList<String> SeparadorTerminos(String cadena) {
		ArrayList<String> array = new ArrayList<String>();
		String operacion = "";

		for (char x : cadena.toCharArray()) {
			if (x != (char) 43 && x != (char) 45)
				operacion += Character.toString(x);
			else {
				array.add(operacion);
				array.add(Character.toString(x));
				operacion = "";
			}
		}

		// cuando no encuentra que mas leer sale, eso deja cargada con el ultimo valor a
		// operacion, por lo cual hay que agregarlo
		if (!operacion.equals("+") && !operacion.equals("-") && !operacion.equals(""))
			array.add(operacion);

		return array;
	}

	private static ArrayList<String> CheckLastElement(ArrayList<String> cadena) {
		// el usuario puede dejar un operador como ultimo dato ingresado, si es asi, lo
		// remuevo.
		int indice = cadena.size() - 1;

		if (cadena.get(indice).equals("+") || cadena.get(indice).equals("-")) {
			cadena.remove(indice);
		}

		return cadena;
	}

	private static ArrayList<String> CheckSubOperacionesDivision(String[] cadena) {
		Calculador calcu = new Calculador();
		ArrayList<String> resultado = new ArrayList<String>();

		for (int i = 0; i < cadena.length; i++) {
			if (cadena[i].contains("/")) {
				String[] split = cadena[i].split("/");

				resultado
						.add(String.valueOf(calcu.Dividir(Double.parseDouble(split[0]), Double.parseDouble(split[1]))));
			} else
				resultado.add(cadena[i]);
		}

		return resultado;
	}

	private static String CheckSubOperacionesMultiplicacion(String[] cadena) {
		Calculador calcu = new Calculador();
		String resultado = "";

		for (int i = 0; i < cadena.length; i++) {
			if (i == 0) {
				resultado = String.valueOf(calcu.Multiplicar(Double.parseDouble(cadena[i]), 1));
			} else
				resultado = String
						.valueOf(calcu.Multiplicar(Double.parseDouble(resultado), Double.parseDouble(cadena[i])));
		}

		return resultado;
	}

	private static String ResolverDivConMul(String cadena) {
		// spliteo divisiones
		String[] Split = cadena.split("\\*");
		ArrayList<String> calculadoDivision = CheckSubOperacionesDivision(Split);

		// resuelvo posibles multiplicaciones dentro del array que converti a lista ya
		// que es mas facil de manejar
		String[] stringArray = calculadoDivision.toArray(new String[0]);
		return CheckSubOperacionesMultiplicacion(stringArray);
	}

	private static String OnliDiv(String[] cadena) {
		Calculador calcu = new Calculador();
		String resultado = "";

		for (int i = 0; i < cadena.length; i++) {
			if (i == 0) {
				resultado = String.valueOf(calcu.Dividir(Double.parseDouble(cadena[i]), 1));
			} else
				resultado = String.valueOf(calcu.Dividir(Double.parseDouble(resultado), Double.parseDouble(cadena[i])));
		}

		return resultado;
	}

	private static String ResolverMultiplicarDividir(String cadena) {
		String resultado = "";

		if (cadena.contains("*")) {
			resultado = ResolverDivConMul(cadena);
		} else {
			String[] Split = cadena.split("/");
			resultado = OnliDiv(Split);
		}

		return resultado;
	}

	private static String ResolveFirstStep(ArrayList<String> cadena) {
		ArrayList<String> array = new ArrayList<String>();

		for (String x : cadena) {
			if (!x.contains("+") && !x.contains("-")) {
				array.add(ResolverMultiplicarDividir(x));
			} else {
				array.add(x);
			}
		}
		String devolver = "";
		
		for(String x: array)
		{
			devolver += x;
		}
		
		return devolver;
	}

	private static String ResolverResta(String[] cadena) {
		Calculador calcu = new Calculador();
		String resultado = "";

		for (int i = 0; i < cadena.length; i++) {
			if (i == 0) {
				resultado = String.valueOf(calcu.Restar(Double.parseDouble(cadena[i]), 0));
			} else
				resultado = String.valueOf(calcu.Restar(Double.parseDouble(resultado), Double.parseDouble(cadena[i])));
		}

		return resultado;
	}

	private static ArrayList<String> CheckSubOperacionesResta(String[] cadena) {
		Calculador calcu = new Calculador();
		ArrayList<String> resultado = new ArrayList<String>();

		for (int i = 0; i < cadena.length; i++) {
			if (cadena[i].contains("-")) {
				String[] split = cadena[i].split("-");

				resultado
						.add(String.valueOf(calcu.Restar(Double.parseDouble(split[0]), Double.parseDouble(split[1]))));
			} else
				resultado.add(cadena[i]);
		}

		return resultado;
	}
	
	private static String CheckSubOperacionesSuma(String[] cadena) {
		Calculador calcu = new Calculador();
		String resultado = "";

		for (int i = 0; i < cadena.length; i++) {
			if (i == 0) {
				resultado = String.valueOf(calcu.Sumar(Double.parseDouble(cadena[i]), 0));
			} else
				resultado = String
						.valueOf(calcu.Sumar(Double.parseDouble(resultado), Double.parseDouble(cadena[i])));
		}

		return resultado;
	}
	
	private static String AddAndSubtract(String cadena) {		
		if (cadena.contains("+")) {
			String[] Split = cadena.split("\\+");
			ArrayList<String> resultadoFinalParcial = new ArrayList<String>(CheckSubOperacionesResta(Split));
			
			String[] stringArray = resultadoFinalParcial.toArray(new String[0]);
			return CheckSubOperacionesSuma(stringArray);
			
		} else {
			return ResolverResta(cadena.split("-"));
		}
	}

	public static int ResolveMath(String cadena) {
		ArrayList<String> array = new ArrayList<String>(SeparadorTerminos(cadena));
		CheckLastElement(array);
		String resultadoParcial = ResolveFirstStep(array);
		String[] resultado = AddAndSubtract(resultadoParcial).split("\\.");
		return Integer.parseInt(resultado[0]);
	}

	public static boolean CheckDivCero(String cadena)
	{
		//se toma el retorno como "Puede continuar?"
		if(cadena.contains("/0"))
			return false;
		else
			return true;
	}
	
	public static boolean CheckResultado(Integer resulcuenta, String numero)
	{
		if(resulcuenta == Integer.parseInt(numero))
			return true;
		else
			return false;
	}
}
